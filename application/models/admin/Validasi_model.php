<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasi_model extends CI_Model {

	function getAllPengajuan()
	{
		return $this->db->get('pengajuan')->result_array();
	}

	function get_detail($id_pengajuan)
	{
		return $this->db->get_where('pengajuan', array('id_pengajuan' => $id_pengajuan))->row_array();
	}

	function getDoc($id)
	{
		return $this->db->get_where('dokumen_pengajuan', array('id_pengajuan' => $id))->result_array();
	}

	function getby_Id($id)
	{
		return $this->db->get_where('dokumen_pengajuan', array('id_dokumen' => $id))->row_array();
	}

	function update($id, $params)
	{
		$this->db->where('id_dokumen', $id);
		return $this->db->update('dokumen_pengajuan', $params);
	}

	function update_status($id, $params)
	{
		$this->db->where('id_pengajuan', $id);
		return $this->db->update('pengajuan', $params);
	}

	function get_matriks($id_pengajuan)
	{
		$this->db->join('ref_matriks_penilaian', 'isian_matriks_penilaian.id_ref_m_penilaian = ref_matriks_penilaian.id_ref_m_penilaian', 'left');
		return $this->db->get_where('isian_matriks_penilaian', array('id_pengajuan' => $id_pengajuan))->row_array();
	}

}

/* End of file Validasi_model.php */
/* Location: ./application/models/admin/Validasi_model.php */