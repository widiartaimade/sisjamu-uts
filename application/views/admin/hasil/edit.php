<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Hasil Assessment</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/hasil_assessment/edit/'.$hasil['id_isian']), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="id_pengajuan" class="col-sm-3 control-label">Pengajuan</label>

                <div class="col-sm-9">
                  <select name="id_pengajuan" class="form-control">
                    <option disabled>-- select pengajuan --</option>
                    <?php foreach ($pengajuan as $key) { ?>
                    <option value="<?php echo $key['id_pengajuan'] ?>" <?= ($hasil['id_pengajuan'] == $k['id_pengajuan'])?'selected': '' ?> ><?php echo $key['id_pengajuan'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="id_ref_m_penilaian" class="col-sm-3 control-label">Referensi</label>

                <div class="col-sm-9">
                  <select name="id_ref_m_penilaian" class="form-control">
                    <option disabled>-- select refernsi --</option>
                    <?php foreach ($refernsi as $key) { ?>
                    <option value="<?php echo $key['id_ref_m_penilaian'] ?>" <?= ($hasil['id_ref_m_penilaian'] == $k['id_ref_m_penilaian'])?'selected': '' ?> ><?php echo $key['id_ref_m_penilaian'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="jawaban" class="col-sm-3 control-label">Jawaban</label>

                <div class="col-sm-9">
                  <textarea name="jawaban" class="form-control" id="jawaban"><?php echo $hasil['jawaban'] ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="ada_file" class="col-sm-3 control-label">File</label>

                <div class="col-sm-9">
                  <select name="ada_file" class="form-control" id="ada_file" onchange="atur(this)">
                    <option>-- select ada file / tidak --</option>
                    <option value="1" <?= ($hasil['ada_file'] == 1)?'selected': '' ?> >Ada File</option>
                    <option value="0" <?= ($hasil['ada_file'] == 0)?'selected': '' ?> >Tidak ada File</option>
                  </select>
                </div>
              </div>
              
              <div class="form-group" id="url_file">
                <label for="url_file" class="col-sm-3 control-label">URL File</label>

                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="file" name="url_file" class="form-control" id="url_file" value="<?= $hasil['url_file'] ?>">
                    <span class="input-group-addon">select</span>
                    <!--
                    <a data-toggle="modal"  href="javascript:;" data-target="#myModal" class="btn input-group-addon" type="button">Select</a>
                    <a href="<?= base_url() ?>public/filemanager/dialog.php?type=2" data-fancybox-type="iframe" class="btn btn-sm btn-info input-group-addon fancy">pilih</a>
                    -->
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="nilai" class="col-sm-3 control-label">Nilai</label>

                <div class="col-sm-9">
                  <input type="text" name="nilai" class="form-control" id="nilai" value="<?= $hasil['nilai'] ?>" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <a class="btn btn-success full-right" href="<?php echo site_url('admin/hasil_assessment/index');?>">
                    Batal
                  </a>
                </div>
                <div class="col-md-6">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div> 
</section> 

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <iframe style="width: 570px; height: 450px;" src="<?= base_url() ?>public/filemanager/dialog.php?type=2&field_id=url_file&relative_url=1" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div> 

<script type="text/javascript">
  $("#mn_hasil").addClass('active');

  function atur(data) {
    var value = data.value;
    if (value === '1') {
      $("#url_file").attr('required', '');
    }else{
      $("#url_file").removeAttr('required', '');
    }
  };
</script>