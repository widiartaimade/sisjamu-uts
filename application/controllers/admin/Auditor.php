<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Auditor_model', 'AM');
	}

	public function index()
	{
		$data['data'] = $this->AM->getAll();
		$data['view'] = 'admin/auditor/index';
		$this->load->view('admin/layout', $data);		
	}

	public function add()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				$params = array(
					'username' => $this->input->post('username'),
					'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'full_name' => $this->input->post('full_name'),
					'telp' => $this->input->post('mobile_no'),
					'email' => $this->input->post('email')
				);

				$params = $this->security->xss_clean($params);
				$data = $this->AM->insert($params);

				if ($data) {
					$this->session->set_flashdata('msg', 'Record is Added Successfully!');
					redirect(base_url('admin/auditor'));
				}
			} else {
				$data['view'] = 'admin/auditor/add';
				$this->load->view('admin/layout', $data);
			}

		}else{
			$data['view'] = 'admin/auditor/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id='')
	{
		$data['auditor'] = $this->AM->get_byId($id);

		if (isset($data['auditor']['id_auditor'])) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				$params = array(
					'username' => $this->input->post('username'),
					'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'full_name' => $this->input->post('full_name'),
					'telp' => $this->input->post('mobile_no'),
					'email' => $this->input->post('email')
				);

				$params = $this->security->xss_clean($params);
				$data = $this->AM->update($id, $params);

				if ($data) {
					$this->session->set_flashdata('msg', 'Record is upfated Successfully!');
					redirect(base_url('admin/auditor'));
				}
			} else {
				$data['view'] = 'admin/auditor/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->flashdata('msg', 'Data you are trying to edit does not exist !');
			redirect(base_url('admin/auditor'));
		}
	}

	public function delete($id)
	{
		$data['auditor'] = $this->AM->get_byId($id);

		if (isset($data['auditor']['id_auditor'])) {
			$this->AM->delete($id);
			$this->session->set_flashdata('msg', 'Record was delete');
			redirect(base_url('admin/auditor'));
		}else{
			$this->session->flashdata('msg', 'Data you are trying to delete does not exist !');
			redirect(base_url('admin/auditor'));	
		}
	}

}

/* End of file Auditor.php */
/* Location: ./application/controllers/admin/Auditor.php */