<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('settings')->result_array();
	}

	public function getLimit()
	{
		$this->db->limit(1);
		return $this->db->get('settings')->row_array();
	}

	public function getby_Id($id)
	{
		$this->db->where('id_setting', $id);
		return $this->db->get('settings')->row_array();
	}

	public function insert($params)
	{
		return $this->db->insert('settings', $params);
	}

	public function update($id, $params)
	{
		$this->db->where('id_setting', $id);
		return $this->db->update('settings', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_setting', $id);
		return $this->db->delete('settings');
	}

}

/* End of file Setting_model.php */
/* Location: ./application/models/admin/Setting_model.php */