<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_assessment extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Hasil_assessment_model', 'HAM');
		$this->load->model('admin/Pengajuan_auditor_model', 'PAM');
		$this->load->model('admin/Ref_penilaian_model', 'RPM');
	}

	public function index()
	{
		$data['hasil_assessment'] = $this->HAM->getAll();
		$data['view'] = 'admin/hasil/index';
		$this->load->view('admin/layout', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('id_pengajuan', 'Pengajuan', 'trim|required');
		$this->form_validation->set_rules('id_ref_m_penilaian', 'Ref Penilaian', 'trim|required');
		$this->form_validation->set_rules('jawaban', 'Jawaban', 'trim|required');
		$this->form_validation->set_rules('ada_file', 'Ada File', 'trim|required');
		$this->form_validation->set_rules('nilai', 'Nilai', 'trim|required');

		if ($this->form_validation->run()) {
			if (!empty($_FILES['url_file']['name']))
            {
                $this->load->library('upload');
                $config['upload_path'] = 'root/hasil_assessment';
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $config['file_name'] = 'hasil-'.$this->input->post('id_ref_m_penilaian');

                $this->upload->initialize($config);
                $this->upload->do_upload('url_file');
                $file = $this->upload->data();

                $url = $config['upload_path'].'/'.$file['file_name'];
                $url_lengkap = base_url().$url;
            }else{
                $url_lengkap = '';
            }

			$params = array(
				'id_pengajuan' => $this->input->post('id_pengajuan'),
				'id_ref_m_penilaian' => $this->input->post('id_ref_m_penilaian'),
				'jawaban' => $this->input->post('jawaban'),
				'ada_file' => $this->input->post('ada_file'),
				'url_file' => $url_lengkap,
				'nilai' => $this->input->post('nilai')
			);

			$this->HAM->insert($params);
			$this->session->set_flashdata('msg', 'Record successfully added');
			redirect('admin/hasil_assessment');
		} else {
			$data['pengajuan'] = $this->PAM->getAll();
			$data['referensi'] = $this->RPM->getAll();
			$data['view'] = 'admin/hasil/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id)
	{
		$data['hasil'] = $this->HAM->getby_Id($id);

		if ($data['hasil']['id_isian']) {
	
			$this->form_validation->set_rules('id_pengajuan', 'Pengajuan', 'trim|required');
			$this->form_validation->set_rules('id_ref_m_penilaian', 'Ref Penilaian', 'trim|required');
			$this->form_validation->set_rules('jawaban', 'Jawaban', 'trim|required');
			$this->form_validation->set_rules('ada_file', 'Ada File', 'trim|required');
			$this->form_validation->set_rules('nilai', 'Nilai', 'trim|required');

			if ($this->form_validation->run()) {
				if (!empty($_FILES['url_file']['name']))
	            {
	                $this->load->library('upload');
	                $config['upload_path'] = 'root/hasil_assessment';
	                $config['allowed_types'] = '*';
	                $config['overwrite'] = TRUE;
	                $config['file_name'] = 'hasil-'.$this->input->post('id_ref_m_penilaian');

	                $this->upload->initialize($config);
	                $this->upload->do_upload('url_file');
	                unlink($config['upload_path'].'/'.$file['file_name']);
	                $file = $this->upload->data();

	                $url = $config['upload_path'].'/'.$file['file_name'];
	                $url_lengkap = base_url().$url;
	            }else{
	                $url_lengkap = '';
	            }

				$params = array(
					'id_pengajuan' => $this->input->post('id_pengajuan'),
					'id_ref_m_penilaian' => $this->input->post('id_ref_m_penilaian'),
					'jawaban' => $this->input->post('jawaban'),
					'ada_file' => $this->input->post('ada_file'),
					'url_file' => $url_lengkap,
					'nilai' => $this->input->post('nilai')
				);

				$this->HAM->update($id, $params);
				$this->session->set_flashdata('msg', 'Record successfully update');
				redirect('admin/hasil_assessment');
			} else {
				$data['pengajuan'] = $this->PAM->getAll();
				$data['referensi'] = $this->RPM->getAll();
				$data['view'] = 'admin/hasil/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'The data you tring to edit does not exist');
			redirect('admin/Hasil_assessment');
		}
	}

	public function delete($id)
	{
		$data['hasil'] = $this->HAM->getby_Id($id);

		if ($data['hasil']['id_isian']) {
			$this->HAM->delete($id);
			unlink($data['hasil']['url_file']);
			$this->session->set_flashdata('msg', 'Data has been delete');
			redirect('admin/hasil_assessment');
		}else{
			$this->session->set_flashdata('msg', 'The data you tring to edit does not exist');
			redirect('admin/hasil_assessment');
		}
	}

}

/* End of file Hasil_assessment.php */
/* Location: ./application/controllers/admin/Hasil_assessment.php */