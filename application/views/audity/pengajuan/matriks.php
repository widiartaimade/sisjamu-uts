<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Matriks Penilain
        <small>Program Studi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pengajuan</a></li>
        <li class="active">Matriks Penilaian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Lengkapi form di bawah ini</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
		  <!-- Form -->
      <?php echo form_open(base_url('audity/pengajuan/submit-matriks/'.$id), 'class="form-horizontal"');  ?>
      <div class="box-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Deskripsi</th>
              <th>Isian</th>
              <th>File</th>
              <th>URL</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($matriks as $m) { ?>
            <tr>
              <td><input type="text" class="form-control" name="id_<?php echo $m['id_ref_m_penilaian'] ?>" value="<?php echo $m['id_ref_m_penilaian'] ?>" readonly=""></td>
              <td><?php echo $m['deskripsi'] ?></td>
              <td><textarea class="form-control" rows="4" name="jwbn_<?php echo $m['id_ref_m_penilaian'] ?>" placeholder="Jawaban Mastriks <?php echo $m['id_ref_m_penilaian'] ?>"></textarea></td>
              <td>
                <select class="form-control" name="file_<?php echo $m['id_ref_m_penilaian'] ?>" id="file_<?php echo $m['id_ref_m_penilaian'] ?>" onchange="exist(this)">
                  <option>-- pilih --</option>
                  <option value="1">Ada</option>
                  <option value="0">Tidak</option>
                </select>
              </td>
              <td><input type="file" name="url_<?php echo $m['id_ref_m_penilaian'] ?>" class="form-control" id="url_<?php echo $m['id_ref_m_penilaian'] ?>" placeholder="URL File"></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="box-footer">
        <button type="button" class="btn btn-default">Cancel</button>
        <button type="submit" class="btn btn-info pull-right">Simpan</button>
      </div>
    </form>
      
    </div>
  </section>
<script>
  function exist(data) {
    var value = data.value;
    if (value === '1') {
      $("#url_template").attr('required', '');
      $("#jenis_file").attr('required', '');
    }else{
      $("#url_template").removeAttr('required', '');
      $("#jenis_file").removeAttr('required', '');
    }
  };
</script>

<script>
  $("#pengajuan-baru").addClass('active');
</script>  
