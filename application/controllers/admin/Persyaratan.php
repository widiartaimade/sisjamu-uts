<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persyaratan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Persyaratan_model', 'PM');
	}

	public function index()
	{
		$data['persyaratan'] = $this->PM->getAll();
		$data['view'] = 'admin/persyaratan/index';
		$this->load->view('admin/layout', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('id_kategori_syarat', 'Kategori Syarat', 'trim|required');
		$this->form_validation->set_rules('nama_syarat', 'Nama Syarat', 'trim|required');
		$this->form_validation->set_rules('ket_syarat', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('ada_template', 'Template', 'trim|required');

		if ($this->form_validation->run()) {
			

			$params = array(
				'id_kategori_syarat' => $this->input->post('id_kategori_syarat'),
				'nama_syarat' => $this->input->post('nama_syarat'),
				'ket_syarat' => $this->input->post('ket_syarat'),
				'ada_template' => $this->input->post('ada_template'),
				'url_template' => $this->input->post('url_template'),
				'jenis_file' => $this->input->post('jenis_file'),
			);

			$this->PM->insert($params);
			$this->session->set_flashdata('msg', 'Data succesfully added');
			redirect('admin/persyaratan');
		} else {
			$this->load->model('admin/Kategori_model', 'KM');
			$data['Kategori'] = $this->KM->getAll();
			$data['view'] = 'admin/persyaratan/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id)
	{
		$data['persyaratan'] = $this->PM->getby_Id($id);
		if ($data['persyaratan']['id_persyaratan']) {
			$this->form_validation->set_rules('id_kategori_syarat', 'Kategori Syarat', 'trim|required');
			$this->form_validation->set_rules('nama_syarat', 'Nama Syarat', 'trim|required');
			$this->form_validation->set_rules('ket_syarat', 'keterangan', 'trim|required');
			$this->form_validation->set_rules('ada_template', 'Template', 'trim|required');

			if ($this->form_validation->run()) {
				/*
				if (!empty($_FILES['url_template']['name']))
	            {
	                $this->load->library('upload');
	                $config['upload_path'] = 'root/template';
	                $config['allowed_types'] = '*';
	                $config['overwrite'] = TRUE;
	                $config['file_name'] = 'template-'.$this->input->post('nama_syarat');

	                $this->upload->initialize($config);
	                $this->upload->do_upload('url_template');
	                unlink($config['upload_path'].'/'.$file['file_name']);
	                $file = $this->upload->data();

	                $url = $config['upload_path'].'/'.$file['file_name'];
	                $url_lengkap = base_url().$url;
	            }else{
	                $url_lengkap = '';
	            }
				*/

				$params = array(
					'id_kategori_syarat' => $this->input->post('id_kategori_syarat'),
					'nama_syarat' => $this->input->post('nama_syarat'),
					'ket_syarat' => $this->input->post('ket_syarat'),
					'ada_template' => $this->input->post('ada_template'),
					'url_template' => $this->input->post('url_template'),
					'jenis_file' => $this->input->post('jenis_file'),
				);

				$this->PM->update($id, $params);
				$this->session->set_flashdata('msg', 'Data succesfully Updarted');
				redirect('admin/persyaratan');
			} else {
				$this->load->model('admin/Kategori_model', 'KM');
				$data['Kategori'] = $this->KM->getAll();
				$data['view'] = 'admin/persyaratan/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'Psersyaratan was you are trying to edit does not exist');
			redirect('admin/persyaratan');
		}
	}

	public function delete($id)
	{
		$data['persyaratan'] = $this->PM->getby_Id($id);
		if ($data['persyaratan']['id_persyaratan']) {
			$this->PM->delete($id);
			unlink($data['persyaratan']['url_template']);
			$this->session->set_flashdata('msg', 'Data succcesfully delete');
			redirect('admin/persyaratan');
		}else{
			$this->session->set_flashdata('msg', 'Data was you are trying to delete does not exist');
			redirect('admin/persyaratan');
		}
	}

	function getext()
    {
        $string = $this->input->get('url_template');
        $ext = pathinfo($string);
        echo $ext['extension'];
    }


    /*
    if (!empty($_FILES['url_template']['name']))
            {
                $this->load->library('upload');
                $config['upload_path'] = 'root/template';
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $config['file_name'] = 'template-'.$this->input->post('nama_syarat');

                $this->upload->initialize($config);
                $this->upload->do_upload('url_template');
                $file = $this->upload->data();

                $url = $config['upload_path'].'/'.$file['file_name'];
                $url_lengkap = base_url().$url;
            }else{
                $url_lengkap = '';
            }
    */

}

/* End of file Persyaratan.php */
/* Location: ./application/controllers/admin/Persyaratan.php */