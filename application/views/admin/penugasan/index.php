<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pemilihan Auditor
        <small>AK dan AL</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pemilihan</a></li>
        <li class="active">List pemilihan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List pemilihan</h3>
              <div class="box-tools">
                <a class="btn btn-info" href="<?php echo site_url('admin/pengajuan_auditor/set_auditor') ?>">Add</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Pengajuan</th>
                  <th>Auditor</th>
                  <th>Tgl Mulai</th>
                  <th>Tgl Selesai</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($pengajuan as $key) { ?>
                    <tr>
                      <td><?php echo $key['id_pengajuan'] ?></td>
                      <td><?php echo $key['full_name'] ?></td>
                      <td><?php echo $key['tgl_mulai_penugasan']; ?></td>
                      <td><?php echo $key['tgl_selesai_penugasan']; ?></td>
                      <td><?php echo $key['status']; ?></td>
                      <td>
                        <a class="btn btn-info" href="<?php echo site_url('admin/pengajuan_auditor/edit/'.$key['id_auditor_pengajuan']) ?>"><i class="fa fa-pencil"></i> Edit</a>
                        <a class="btn btn-danger" href="<?php echo site_url('admin/pengajuan_auditor/delete/'.$key['id_auditor_pengajuan']) ?>"><i class="fa fa-trash"></i> Hapus</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#mn_penugasan").addClass('active');
</script>  
