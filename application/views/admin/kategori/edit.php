<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add New Kategori</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/kategori/edit/'.$kategori['id_kategori_syarat']), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="nama_kategori" class="col-sm-2 control-label">Nama Kategori</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_kategori" value="<?php echo ($this->input->post('nama_kategori') ? $this->input->post('nama_kategori') : $kategori['nama_kategori']); ?>" class="form-control" id="nama_kategori" placeholder="Nama Kategori Syarat">
                </div>
              </div>
              <div class="form-group">
                <label for="ket_kategori" class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-9">
                  <input type="text" name="ket_kategori" value="<?php echo ($this->input->post('ket_kategori') ? $this->input->post('ket_kategori') : $kategori['ket_kategori']); ?>" class="form-control" id="ket_kategori" placeholder="Keterangan kategori">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_kategori").addClass('active');
</script>