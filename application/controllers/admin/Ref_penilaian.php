<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_penilaian extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Ref_penilaian_model', 'RPM');
	}

	public function index()
	{
		$data['ref_penilaian'] = $this->RPM->getAll();
		$data['view'] = 'admin/penilaian/index';
		$this->load->view('admin/layout', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('standar', 'Standar', 'trim|required');
		$this->form_validation->set_rules('butir', 'Butir', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

		if ($this->form_validation->run()) {
			$params = array(
				'standar' => $this->input->post('standar'),
				'butir' => $this->input->post('butir'),
				'deskripsi' => $this->input->post('deskripsi')
			);

			$this->RPM->insert($params);
			$this->session->set_flashdata('msg', 'Record successfully added');
			redirect('admin/ref_penilaian');
		} else {
			$data['view'] = 'admin/penilaian/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id)
	{
		$data['ref_penilaian'] = $this->RPM->getby_Id($id);

		if ($data['ref_penilaian']['id_ref_m_penilaian']) {

			$this->form_validation->set_rules('standar', 'Standar', 'trim|required');
			$this->form_validation->set_rules('butir', 'Butir', 'trim|required');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

			if ($this->form_validation->run()) {
				$params = array(
					'standar' => $this->input->post('standar'),
					'butir' => $this->input->post('butir'),
					'deskripsi' => $this->input->post('deskripsi')
				);

				$this->RPM->update($id, $params);
				$this->session->set_flashdata('msg', 'Record successfully updated');
				redirect('admin/ref_penilaian');
			} else {
				$data['view'] = 'admin/penilaian/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'The data you can try edit does not exist');
			redirect('admin/ref_penilaian');
		}
	}

	public function delete($id)
	{
		$data['ref_penilaian'] = $this->RPM->getby_Id($id);

		if ($data['ref_penilaian']['id_ref_m_penilaian']) {
			$this->RPM->delete($id);
			$this->session->set_flashdata('msg', 'Data has been delete');
			redirect('admin/ref_penilaian');
		}else{
			$this->session->set_flashdata('msg', 'The data you can try edit does not exist');
			redirect('admin/ref_penilaian');
		}
	}

}

/* End of file Ref_penilaian.php */
/* Location: ./application/controllers/admin/Ref_penilaian.php */