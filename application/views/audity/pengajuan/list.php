<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Semua Pengajuan
        <small>audity</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pengajuan</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Semua Pengajuan</h3>
            </div>
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Jenis Akreditasi</th>
                  <th>Jenjang</th>
                  <th>Prodi</th>
                  <th>No Surat</th>
                  <th>Tgl Surat</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($pengajuan as $p) { ?>
                    <tr>
                      <td><?php echo $p['jenis_akreditasi'] ?></td>
                      <td><?php echo $p['jenjang'] ?></td>
                      <td><?php echo $p['prodi'] ?></td>
                      <td><?php echo $p['no_srt_pengantar'] ?></td>
                      <td><?php echo date('d F Y', strtotime($p['tgl_srt_pengantar'])) ?></td>
                      <td><label class="label label-info"><?php echo $p['status_pengajuan'] ?></label></td>
                      <td>
                        <?php if ($p['status_pengajuan'] == 'Submit Dokumen' || $p['status_pengajuan'] == 'Revisi Dokumen') { ?>
                          <a class="btn btn-sm btn-info" title="Edit" href="<?php echo site_url('audity/pengajuan/edit/'.$p['id_pengajuan']) ?>"><i class="fa fa-edit"></i></a>
                          <a class="btn btn-sm btn-info" title="Hapus" href="<?php echo site_url('audity/pengajuan/remove/'.$p['id_pengajuan']) ?>"><i class="fa fa-trash"></i></a>
                          <a class="btn btn-sm btn-info" title="Upload Dokumen" href="<?php echo site_url('audity/pengajuan/upload/'.$p['id_pengajuan']) ?>"><i class="fa fa-upload"></i></a>
                          <a class="btn btn-sm btn-info" title="Matriks Penilaian" href="<?php echo site_url('audity/pengajuan/matriks/'.$p['id_pengajuan']) ?>"><i class="fa fa-bar-chart"></i></a>
                        <?php } ?>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#pengajuan-list").addClass('active');
</script>  
