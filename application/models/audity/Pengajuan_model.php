<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_model extends CI_Model {

	function get_all_pengajuan()
	{
		return $this->db->get('pengajuan')->result_array();
	}

	function insert($params)
	{
		return $this->db->insert('pengajuan', $params);
	}

	function update($id, $params)
	{
		$this->db->where('id_pengajuan', $id);
		return $this->db->update('pengajuan', $params);
	}

	function remove($id)
	{
		$this->db->where('id_pengajuan', $id);
		return $this->db->delete('pengajuan');
	}

	function get_by_id($id)
	{
		return $this->db->get_where('pengajuan', array('id_pengajuan' => $id ))->row_array();
	}

	function get_by_audity($id)
	{
		return $this->db->get_where('pengajuan', array('id_audity' => $id))->result_array();
	}

}

/* End of file Pengajuan_model.php */
/* Location: ./application/models/audity/Pengajuan_model.php */