<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Referensi Matriks
        <small>Penilaian</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Ref Penilaian</a></li>
        <li class="active">List ref</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List ref penilaian</h3>
              <div class="box-tools">
                <a class="btn btn-info" href="<?php echo site_url('admin/ref_penilaian/add') ?>">Add</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Ref</th>
                  <th>Standar</th>
                  <th>Butir</th>
                  <th>Deskripsi</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($ref_penilaian as $key) { ?>
                    <tr>
                      <td><?php echo $key['id_ref_m_penilaian'] ?></td>
                      <td><?php echo $key['standar'] ?></td>
                      <td><?php echo $key['butir']; ?></td>
                      <td><?php echo $key['deskripsi']; ?></td>
                      <td>
                        <a class="btn btn-info" href="<?php echo site_url('admin/ref_penilaian/edit/'.$key['id_ref_m_penilaian']) ?>"><i class="fa fa-pencil"></i> Edit</a>
                        <a class="btn btn-danger" href="<?php echo site_url('admin/ref_penilaian/delete/'.$key['id_ref_m_penilaian']) ?>"><i class="fa fa-trash"></i> Hapus</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $("#mn_matriks").addClass('active');
</script>  
