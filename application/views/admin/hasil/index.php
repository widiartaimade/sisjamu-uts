<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hasil Assessment
        <small>Assessment</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Hasil Assessment</a></li>
        <li class="active">List hasil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List hasil</h3>
              <div class="box-tools">
                <a class="btn btn-info" href="<?php echo site_url('admin/hasil_assessment/add') ?>">Add</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Pengajuan</th>
                  <th>ID Ref</th>
                  <th>Jawaban</th>
                  <th>Ada File</th>
                  <th>URL FIle</th>
                  <th>Nilai</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($hasil_assessment as $key) { ?>
                    <tr>
                      <td><?php echo $key['id_pengajuan'] ?></td>
                      <td><?php echo $key['id_ref_m_penilaian'] ?></td>
                      <td><?php echo $key['jawaban']; ?></td>
                      <td><?php echo $key['ada_file']; ?></td>
                      <td><?php echo $key['url_file']; ?></td>
                      <td><?php echo $key['Nilai']; ?></td>
                      <td>
                        <a class="btn btn-info" href="<?php echo site_url('admin/hasil_assessment/edit/'.$key['id_isian']) ?>"><i class="fa fa-pencil"></i> Edit</a>
                        <a class="btn btn-danger" href="<?php echo site_url('admin/hasil_assessment/delete/'.$key['id_isian']) ?>"><i class="fa fa-trash"></i> Hapus</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#mn_hasil").addClass('active');
</script>  
