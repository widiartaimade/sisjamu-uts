<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add New Periode</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/periode/add'), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="nama_periode" class="col-sm-2 control-label">Nama periode</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_periode" class="form-control" id="nama_periode" placeholder="Nama Periode">
                </div>
              </div>
              <div class="form-group">
                <label for="buka" class="col-sm-2 control-label">Buka</label>
                <div class="col-sm-9">
                  <input type="date" name="buka" class="form-control" id="buka">
                </div>
              </div>
              <div class="form-group">
                <label for="tutup" class="col-sm-2 control-label">Tutup</label>
                <div class="col-sm-9">
                  <input type="date" name="tutup" class="form-control" id="tutup">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_periode").addClass('active');
$("#add_periode").addClass('active');
</script>    