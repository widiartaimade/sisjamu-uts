<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Validasi
        <small>Dokumen kelengkapan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Validasi</a></li>
        <li class="active">List pengajuan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Pengajuan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Pengajuan</th>
                  <th>Jenis Akreditasi</th>
                  <th>Jenjang</th>
                  <th>Prodi</th>
                  <th>Status Pengajuan</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($pengajuan as $p) { ?>
                    <tr>
                      <td><?= $p['id_pengajuan'] ?></td>
                      <td><?= $p['jenis_akreditasi'] ?></td>
                      <td><?= $p['jenjang'] ?></td>
                      <td><?= $p['prodi'] ?></td>
                      <td><label class="label label-info"><?= $p['status_pengajuan'] ?></label></td>
                      <td>
                        <a class="btn btn-xs btn-info" href="<?php echo site_url('admin/pengajuan/detail/'.$p['id_pengajuan']) ?>">Detail</a>
                        <a class="btn btn-xs btn-info" href="<?php echo site_url('admin/pengajuan/matriks/'.$p['id_pengajuan']) ?>">Matriks</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#mn_validasi").addClass('active');
</script>  
