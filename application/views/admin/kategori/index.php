 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  

 <section class="content">
   <div class="box">
    <div class="box-header">
      <h3 class="box-title">Kategori Syarat</h3>
      <div class="box-tools">
        <a class="btn btn-info float-right" href="<?php echo site_url('admin/kategori/add') ?>">Add</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>ID</th>
          <th>Nama Kategori</th>
          <th>Keteragan</th>
          <th style="width: 150px;" class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($kategori as $row): ?>
          <tr>
            <td><?= $row['id_kategori_syarat']; ?></td>
            <td><?= $row['nama_kategori']; ?></td>
            <td><?= $row['ket_kategori']; ?></td>
            <td class="text-right"><a href="<?= base_url('admin/kategori/edit/'.$row['id_kategori_syarat']); ?>" class="btn btn-info btn-flat">Edit</a><a href="<?= base_url('admin/kategori/delete/'.$row['id_kategori_syarat']); ?>" class="btn btn-danger btn-flat">Delete</a></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $("#mn_kategori").addClass('active');
  $("#view_kategori").addClass('active');
</script>        
