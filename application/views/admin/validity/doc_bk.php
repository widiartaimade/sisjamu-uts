<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Validasi
        <small>Dokumen kelengkapan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Validasi</a></li>
        <li class="active">List dokumen</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Dokumen</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Dokumen</th>
                  <th>ID Persyaratan</th>
                  <th>Tgl Upload</th>
                  <th>Nama Dokumen</th>
                  <th>URL</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($dokumen as $d) { ?>
                    <tr>
                      <td><?= $d['id_dokumen'] ?></td>
                      <td><?= $d['id_perysaratan'] ?></td>
                      <td><?= $d['tgl_upload'] ?></td>
                      <td><?= $d['nama_dokumen'] ?></td>
                      <td><a href="<?= base_url(''.$d['url_file']) ?>"><i class="fa fa-download"></i></a></td>
                      <td><p class="label label-info"><?= $d['status_dokumen'] ?></p></td>
                      <td>
                        <a href="<?php echo site_url('admin/validations/revisi/'.$d['id_dokumen']) ?>" class="btn btn-xs btn-danger <?= ($d['status_dokumen'] == 'revisi')? 'disabled': ''?>">Revisi</a>
                        <a href="<?php echo site_url('admin/validations/verifikasi/'.$d['id_dokumen']) ?>" class="btn btn-xs btn-info <?= ($d['status_dokumen'] == 'ok')? 'disabled': ''?>">OK</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer">
              <a class="btn btn-sm btn-info" href="<?php echo site_url('admin/validations') ?>">Back</a>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#mn_validasi").addClass('active');
</script>  
