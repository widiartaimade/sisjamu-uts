<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Periode_model', 'PM');
	}

	public function index()
	{
		$data['periode'] = $this->PM->get_all_periode();
		$data['view'] = 'admin/periode/index';
		$this->load->view('admin/layout', $data);
	}

	public function add(){
		$this->form_validation->set_rules('nama_periode', 'Nama Periode', 'trim|required');
		$this->form_validation->set_rules('buka', 'Tanggal Buka', 'trim|required');
		$this->form_validation->set_rules('tutup', 'Tanggal Tutup', 'trim|required');

		if ($this->form_validation->run()) {
			$data = array(
				'nama_periode' => $this->input->post('nama_periode'),
				'buka' => $this->input->post('buka'),
				'tutup' => $this->input->post('tutup'),
			);
			$data = $this->security->xss_clean($data);
			$result = $this->PM->insert($data);
			if($result){
				$this->session->set_flashdata('msg', 'Record is Added Successfully!');
				redirect(base_url('admin/periode'));
			}
		}
		else{
			$data['view'] = 'admin/periode/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id = 0){
		if($this->input->post('submit')){
			$this->form_validation->set_rules('nama_periode', 'Nama Periode', 'trim|required');
			$this->form_validation->set_rules('buka', 'Tanggal Buka', 'trim|required');
			$this->form_validation->set_rules('tutup', 'Tanggal Tutup', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['periode'] = $this->PM->get_byId($id);
				$data['view'] = 'admin/periode/edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				$data = array(
					'nama_periode' => $this->input->post('nama_periode'),
					'buka' => $this->input->post('buka'),
					'tutup' => $this->input->post('tutup'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->PM->update($data, $id);
				if($result){
					$this->session->set_flashdata('msg', 'Record is Updated Successfully!');
					redirect(base_url('admin/periode'));
				}
			}
		}else{
			$data['periode'] = $this->PM->get_byId($id);
			$data['view'] = 'admin/periode/edit';
			$this->load->view('admin/layout', $data);
		}
	}

	public function delete($id = 0){
		$this->PM->delete($id);
		$this->session->set_flashdata('msg', 'Record is Deleted Successfully!');
		redirect(base_url('admin/periode'));
	}

}

/* End of file Periode.php */
/* Location: ./application/controllers/admin/Periode.php */