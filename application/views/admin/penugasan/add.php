<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Set Auditor</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/pengajuan_auditor/set_auditor'), 'class="form-horizontal"');  ?>
              <div class="form-group">
                <label for="id_pengajuan" class="col-sm-2 control-label">Pengajuan</label>

                <div class="col-sm-4">
                  <select name="id_pengajuan" class="form-control">
                    <option disabled>-- select pengajuan --</option>
                    <?php foreach ($pengajuan as $key) { ?>
                    <option value="<?php echo $key['id_pengajuan'] ?>"><?php echo $key['id_pengajuan'] ?></option>
                    <?php } ?>
                  </select>
                </div>

                <label for="id_auditor" class="col-sm-2 control-label">Auditor</label>

                <div class="col-sm-4">
                  <select name="id_auditor" class="form-control">
                    <option disabled>-- select auditor --</option>
                    <?php foreach ($auditor as $key) { ?>
                    <option value="<?php echo $key['id_auditor'] ?>"><?php echo $key['full_name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai</label>

                <div class="col-sm-4">
                  <input type="date" name="tgl_mulai" class="form-control" id="tgl_mulai" placeholder="">
                </div>

                <label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai</label>

                <div class="col-sm-4">
                  <input type="date" name="tgl_selesai" class="form-control" id="tgl_selesai" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-12">
                  <button class="btn btn-danger" onclick="goBack()">Back</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_penugasan").addClass('active');
</script>    