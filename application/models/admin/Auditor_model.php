<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('auditor')->result_array();
	}

	public function insert($value)
	{
		return $this->db->insert('auditor', $value);
	}

	public function get_byId($value)
	{
		$this->db->where('id_auditor', $value);
		return $this->db->get('auditor')->row_array();
	}

	public function update($id, $value)
	{
		$this->db->where('id_auditor', $id);
		return $this->db->update('auditor', $value);
	}

	public function delete($id)
	{
		$this->db->where('id_auditor', $id);
		return $this->db->delete('auditor');
	}
}

/* End of file Auditor_model.php */
/* Location: ./application/models/admin/Auditor_model.php */