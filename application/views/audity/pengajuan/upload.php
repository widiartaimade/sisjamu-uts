<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Kelengkapan Dokumen</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
	
  <?php echo form_open_multipart(base_url('audity/pengajuan/submit-dokumen/'.$id), 'class="form-horizontal"');  ?> 
		<div class="box-body">
    <?php if(isset($msg) || validation_errors() !== ''): ?>
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            <?= validation_errors();?>
            <?= isset($msg)? $msg: ''; ?>
        </div>
      <?php endif; ?>
      <div style="display: none;">
        <input hidden="true" type="text" name="id" value="<?php echo $id ?>" class="form-control">
      </div>
      
      <table class="table table-hover  table-responsive no-padding">
        <tr>
          <th>Nama Dokumen</th>
          <th>Template</th>
			    <th>Action</th>
        </tr>
        <?php $no=1; foreach ($syarat as $s) { ?>
          <tr>
            <td style="display: none;">
              <input hidden="true" type="text" name="id_persyaratan" value="<?= $s['id_persyaratan'] ?>" class="form-control">
            </td>
            <td><?= $s['nama_syarat']; ?></td>
            <td>
              <?php if ($s['ada_template'] == true) { ?>
                <a class="btn btn-info" title="Download Template" href="<?= $s['url_template'] ?>"><i class="fa fa-download"></i></a>
              <?php } ?>
            </td>
			      <td>
              <input type="file" name="file<?= $no++ ?>" value="<?php echo $this->input->post('file'.$no); ?>" class="form-control" required>
            </td>
          </tr>
        <?php } ?>
      </table>
    </div>

    <div class="box-footer">
      <button type="submit" class="btn btn-info pull-right">Upload</button>
    </div>
  </form>
</div>