<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit New Auditor</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/auditor/edit/'.$auditor['id_auditor']), 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="full_name" class="col-sm-2 control-label">Full Name</label>
                <div class="col-sm-9">
                  <input type="text" name="full_name" value="<?php echo ($this->input->post('full_name') ? $this->input->post('full_name') : $auditor['full_name']); ?>" class="form-control" id="full_name" placeholder="Full Name">
                </div>
              </div>
              <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-9">
                  <input type="text" name="username" value="<?php echo ($this->input->post('username') ? $this->input->post('username') : $auditor['username']); ?>" class="form-control" id="username" placeholder="Username">
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-9">
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <label for="mobile_no" class="col-sm-2 control-label">Telp</label>

                <div class="col-sm-9">
                  <input type="number" name="mobile_no" value="<?php echo ($this->input->post('mobile_no') ? $this->input->post('mobile_no') : $auditor['telp']); ?>" class="form-control" id="mobile_no" placeholder="6281234567890">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>

                <div class="col-sm-9">
                  <input type="email" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $auditor['email']); ?>" class="form-control" id="email" placeholder="xxxxx@xxxx.xx">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_users").addClass('active');
$("#add_user").addClass('active');
</script>    