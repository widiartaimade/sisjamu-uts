<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_model extends CI_Model {

	function insert($params)
	{
		return $this->db->insert('dokumen_pengajuan', $params);
	}

}

/* End of file Dokumen_model.php */
/* Location: ./application/models/audity/Dokumen_model.php */