<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add New Persyaratan</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/persyaratan/add'), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label">Kategori</label>

                <div class="col-sm-9">
                  <select class="form-control" name="id_kategori_syarat">
                    <option disabled>-- pilih --</option>
                    <?php foreach ($Kategori as $k) { ?>
                      <option value="<?php echo $k['id_kategori_syarat'] ?>"><?php echo $k['nama_kategori'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="nama_syarat" class="col-sm-2 control-label">Nama Syarat</label>

                <div class="col-sm-9">
                  <input type="text" name="nama_syarat" class="form-control" id="nama_syarat" placeholder="Nama Syarat">
                </div>
              </div>

              <div class="form-group">
                <label for="ket_syarat" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-9">
                  <input type="text" name="ket_syarat" class="form-control" id="ket_syarat" placeholder="Keterangan Syarat">
                </div>
              </div>
              <div class="form-group">
                <label for="mobile_no" class="col-sm-2 control-label">Template</label>

                <div class="col-sm-9">
                  <select class="form-control" name="ada_template" id="ada_template" onchange="atur(this)">
                    <option>-- pilih --</option>
                    <option value="1">Ada</option>
                    <option value="0">Tidak</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="url_template" class="col-sm-2 control-label">URL Template</label>

                <div class="col-sm-9">
                  <!--
                  <input type="file" name="url_template" class="form-control" id="url_template" placeholder="URL Template">
                -->
                <textarea class="form-control" id="url_template" name="url_template"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="jenis_file" class="col-sm-2 control-label">Jenis File</label>

                <div class="col-sm-9">
                  <input type="text" name="jenis_file" class="form-control" id="jenis_file" placeholder="Jenis File">
                </div>
              </div>
              <!--
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
              -->
              <div class="form-group">
                <div class="col-md-11">
                  <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 

<script src="<?php echo site_url('public/plugins/ckeditor/ckeditor.js') ?>"></script>
<script type="text/javascript">
  $(function () {
      CKEDITOR.replace('url_template',{
          //filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
          //filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
          //filebrowserImageBrowseUrl : '<?php echo base_url('public/kcfinder/browse.php');?>',
          //height: '400px'             
      });
  });
</script>
<script>
  $("#mn_syarat").addClass('active');
  $("#add_syarat").addClass('active');

  function atur(data) {
    var value = data.value;
    if (value === '1') {
      $("#jenis_file").attr('required', '');
    }else{
      $("#jenis_file").removeAttr('required', '');
    }
  };
</script>
<!--
<script type="text/javascript">
  $(function() {
    $("#url_template").change(function(){
      var url_template = $("#url_template").val();
      $.ajax({
        url: '<?php echo site_url("admin/persyaratan/getext"); ?>',
        data: 'url_template': url_template,
        type: 'GET',
        success: function(result) {
          $("#jenis_file").val(result);
        } 
      });
    });
  });
</script>
-->