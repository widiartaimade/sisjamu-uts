<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audity_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('audity')->result_array();
	}

	public function insert($value)
	{
		return $this->db->insert('audity', $value);
	}

	public function get_byId($value)
	{
		$this->db->where('id_audity', $value);
		return $this->db->get('audity')->row_array();
	}

	public function update($id, $value)
	{
		$this->db->where('id_audity', $id);
		return $this->db->update('audity', $value);
	}

	public function delete($id)
	{
		$this->db->where('id_audity', $id);
		return $this->db->delete('audity');
	}

}

/* End of file Audity_model.php */
/* Location: ./application/models/admin/Audity_model.php */