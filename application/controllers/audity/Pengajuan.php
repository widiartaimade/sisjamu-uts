<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pengajuan extends Audity_Contoller {
		public function __construct(){
			parent::__construct();
			$this->load->model('audity/Pengajuan_model', 'PM');
			$this->load->model('audity/Dokumen_model', 'DM');
			$this->load->model('admin/Ref_penilaian_model', 'RPM');
		}

		function index(){
			$data['pengajuan'] = $this->PM->get_by_audity($this->session->userdata('audity_id'));
			$data['view'] = 'audity/pengajuan/list';
			$this->load->view('audity/layout', $data);
		}
	
		function baru(){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('jenis_akreditasi', 'Jenis Akreditas', 'required');
			$this->form_validation->set_rules('jenjang', 'Jenjang', 'required');
			$this->form_validation->set_rules('prodi', 'Prodi', 'required');

			if ($this->form_validation->run()) {
				
				$params = array(
					'id_audity' => $this->session->userdata('audity_id'),
					'jenis_akreditasi' => $this->input->post('jenis_akreditasi'),
					'jenjang' => $this->input->post('jenjang'),
					'prodi' => $this->input->post('prodi'),
					'mode_pembelajaran' => $this->input->post('mode_pembelajaran'),
					'bidang_ilmu' => $this->input->post('bidang_ilmu'),
					'no_srt_pengantar' => $this->input->post('no_srt_pengantar'),
					'tgl_srt_pengantar' => $this->input->post('tgl_srt_pengantar'),
					'alamat' => $this->input->post('alamat'),
					'status_pengajuan' => 'Submit Dokumen'
				);

				$pengajuan = $this->PM->insert($params);
				$this->session->set_flashdata('msg', 'Berhasil menambahkan pengajuan baru');
				redirect('audity/pengajuan/list');

			} else {
				$data['view'] = 'audity/pengajuan/baru';
				$this->load->view('audity/layout', $data);
			}
		}

		function submit_dokumen($id)
		{
			$this->load->model('admin/Persyaratan_model');
			$syarat = $this->Persyaratan_model->getAll();

			for($i=1;$i<=count($syarat);$i++){
                $this->load->library('upload');
                $config['upload_path'] = 'root/berkas';
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $config['file_name'] = 'berkas-'.$this->session->userdata('name').'-'.$i;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('file'.$i)){
					echo $this->upload->display_errors();
				}
				else{
	                $file = $this->upload->data();
	                $url = $config['upload_path'].'/'.$file['file_name'];

	                $params = array(
	                	'id_pengajuan' => $id,
	                	'id_perysaratan' => $this->input->post('id_persyaratan'),
	                	'tgl_upload' => date('Y-m-d'),
	                	'nama_dokumen' => $file['file_name'],
	                	'status_dokumen' => 'unverify',
	                	'url_file' => $url
	                );

	                $this->DM->insert($params);
				}
			}			
            $this->session->set_flashdata('msg', 'berkas berhasil diupload');
            redirect('audity/pengajuan/list');
		}

		function upload($id)
		{			
			$this->load->model('admin/Persyaratan_model');
			$data['id'] = $id;
			$data['syarat'] = $this->Persyaratan_model->getAll();
			$data['view'] = 'audity/pengajuan/upload';
			$this->load->view('audity/layout', $data);
		}

		function matriks($id)
		{			
			$data['id'] = $id;
			$data['matriks'] = $this->RPM->getAll();
			$data['view'] = 'audity/pengajuan/matriks';
			$this->load->view('audity/layout', $data);
		}

		function submit_matriks($id)
		{
			$this->load->library('form_validation');
			$data = $this->RPM->getAll();

			$sukses = 0;
			$error = 0;

			for ($i=1; $i <=count($data); $i++) {
				$this->form_validation->set_rules('id_'.$i, 'Matriks ke-'.$i, 'required');
				$this->form_validation->set_rules('jwbn_'.$i, 'Jawaban ke-'.$i, 'required');
				$this->form_validation->set_rules('file_'.$i, 'File ke-'.$i, 'required');

				$params = array(
					'id_pengajuan' => $id,
					'id_ref_m_penilaian' => $this->input->post('id_'.$i),
					'jawaban' => $this->input->post('jwbn_'.$i),
					'ada_file' => $this->input->post('file_'.$i),
					'url_file' => $this->input->post('url_'.$i)
				);

				$this->load->model('audity/Isian_model');
				$run = $this->Isian_model->simpan($params);

				if ($run) {
					$sukses+1;
				}else{
					$error+1; 
				}
			}

			$this->session->set_flashdata('msg', 'Sukses menyimpan '.$sukses.' data dan '.$error.' gagal');
			redirect('audity/pengajuan/index');
		}
	}

?>	