<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_aktif
{
	protected $CI;

	public function __construct()
	{
        $this->CI =& get_instance();
        $this->CI->load->model('admin/Setting_model', 'SeMo');
        $this->CI->load->model('admin/Periode_model', 'PeMo');
	}

	function aktif($tgl)
	{
		$a = $this->CI->SeMo->getLimit();
		if (!empty($a)) {
			$b = $this->CI->PeMo->getby_Id($a['value_int']);
			if (!empty($b)) {
				return $this->CI->PeMo->get_periode_aktif($tgl);
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	

}

/* End of file periode_aktif.php */
/* Location: ./application/libraries/periode_aktif.php */
