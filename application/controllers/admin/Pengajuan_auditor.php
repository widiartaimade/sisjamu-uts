<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_auditor extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Pengajuan_auditor_model', 'PAM');
		$this->load->model('admin/Auditor_model', 'AM');
	}

	public function index()
	{
		$data['pengajuan'] = $this->PAM->getAll();
		$data['view'] = 'admin/penugasan/index';
		$this->load->view('admin/layout', $data);
	}

	public function set_auditor()
	{
		$this->form_validation->set_rules('id_pengajuan', 'Pengajuan', 'trim|required');
		$this->form_validation->set_rules('id_auditor', 'Auditor', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');

		if ($this->form_validation->run()) {
			$params = array(
				'id_pengajuan' => $this->input->post('id_pengajuan'),
				'id_auditor' => $this->input->post('id_auditor'),
				'tgl_mulai_penugasan' => $this->input->post('tgl_mulai'),
				'tgl_selesai_penugasan' => $this->input->post('tgl_selesai'),
				'status' => 'wait'
			);

			$this->PAM->insert($params);
			$this->session->set_flashdata('msg', 'Auditor has been send');
			redirect('admin/pengajuan_auditor');
		} else {
			$data['pengajuan'] = $this->PAM->getPengajuan();
			$data['auditor'] = $this->AM->getAll();
			$data['view'] = 'admin/penugasan/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id)
	{
		$data['set_auditor'] = $this->PAM->getby_Id($id);

		if ($data['set_auditor']['id_auditor_pengajuan']) {
			$this->form_validation->set_rules('id_pengajuan', 'Pengajuan', 'trim|required');
			$this->form_validation->set_rules('id_auditor', 'Auditor', 'trim|required');
			$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');

			if ($this->form_validation->run()) {
				$params = array(
					'id_pengajuan' => $this->input->post('id_pengajuan'),
					'id_auditor' => $this->input->post('id_auditor'),
					'tgl_mulai_penugasan' => $this->input->post('tgl_mulai'),
					'tgl_selesai_penugasan' => $this->input->post('tgl_selesai'),
					'status' => 'wait'
				);

				$this->PAM->update($id, $params);
				$this->session->set_flashdata('msg', 'Auditor has been send');
				redirect('admin/Pengajuan_auditor');
			} else {
				$data['pengajuan'] = $this->PAM->getPengajuan();
				$data['auditor'] = $this->AM->getAll();
				$data['view'] = 'admin/penugasan/add';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'Record you are trying to edit does not exist');
			redirect('admin/pengajuan_auditor');
		}
	}

	public function delete($id)
	{
		
		$data['set_auditor'] = $this->PAM->getby_Id($id);

		if ($data['set_auditor']['id_auditor_pengajuan']) {
			$this->PAM->delete($id);
			$this->session->set_flashdata('msg', 'Data has been deleted');
			redirect('admin/pengajuan_auditor');
		}else{
			$this->session->set_flashdata('msg', 'Record you are trying to delete does not exist');
			redirect('admin/pengajuan_auditor');
		}
	}

}

/* End of file Penugasan.php */
/* Location: ./application/controllers/admin/Penugasan.php */