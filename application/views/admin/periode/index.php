 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  

 <section class="content">
   <div class="box">
    <div class="box-header">
      <h3 class="box-title">Periode</h3>
      <div class="box-tools">
        <a class="btn btn-info float-right" href="<?php echo site_url('admin/periode/add') ?>">Add</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>Name</th>
          <th>Buka</th>
          <th>Tutup</th>
          <th style="width: 150px;" class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($periode as $row): ?>
          <tr>
            <td><?= $row['nama_periode']; ?></td>
            <td><?= date('d F Y', strtotime($row['buka'])); ?></td>
            <td><?= date('d F Y', strtotime($row['tutup'])); ?></td>
            <td class="text-right"><a href="<?= base_url('admin/periode/edit/'.$row['id_periode']); ?>" class="btn btn-info btn-flat">Edit</a><a href="<?= base_url('admin/periode/del/'.$row['id_periode']); ?>" class="btn btn-danger btn-flat">Delete</a></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $("#mn_periode").addClass('active');
  $("#view_periode").addClass('active');
</script>        
