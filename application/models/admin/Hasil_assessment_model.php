<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_assessment_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('isian_matriks_penilaian')->result_array();
	}

	public function getby_Id($id)
	{
		return $this->db->get_where('isian_matriks_penilaian', array('id_isian', $id))->row_array();
	}

	public function insert($params)
	{
		return $this->db->insert('isian_matriks_penilaian', $params);
	}

	public function update($id, $params)
	{
		$this->db->where('id_isian', $id);
		return $this->db->update('isian_matriks_penilaian', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_isian', $id);
		return $this->db->delete('isian_matriks_penilaian');
	}

}

/* End of file Hasil_assessment_model.php */
/* Location: ./application/models/admin/Hasil_assessment_model.php */