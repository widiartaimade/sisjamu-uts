<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Isian_model extends CI_Model {

	private $tabel = 'isian_matriks_penilaian';

	function simpan($params)
	{
		return $this->db->insert($this->tabel, $params);
	}	

}

/* End of file Isian_model.php */
/* Location: ./application/models/audity/Isian_model.php */