<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit New Persyaratan</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/persyaratan/edit/'.$persyaratan['id_persyaratan']), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label">Kategori</label>

                <div class="col-sm-9">
                  <select class="form-control" name="id_kategori_syarat">
                    <option disabled>-- pilih --</option>
                    <?php foreach ($Kategori as $k) { ?>
                      <option value="<?php echo $k['id_kategori_syarat'] ?>" <?= ($persyaratan['id_kategori_syarat'] == $k['id_kategori_syarat'])?'selected': '' ?> ><?php echo $k['nama_kategori'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="nama_syarat" class="col-sm-2 control-label">Nama Syarat</label>

                <div class="col-sm-9">
                  <input type="text" name="nama_syarat" value="<?php echo ($this->input->post('nama_syarat') ? $this->input->post('nama_syarat') : $persyaratan['nama_syarat']); ?>" class="form-control" id="nama_syarat" placeholder="Nama Syarat">
                </div>
              </div>

              <div class="form-group">
                <label for="ket_syarat" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-9">
                  <input type="text" name="ket_syarat" value="<?php echo ($this->input->post('ket_syarat') ? $this->input->post('ket_syarat') : $persyaratan['ket_syarat']); ?>" class="form-control" id="ket_syarat" placeholder="Keterangan Syarat">
                </div>
              </div>
              <div class="form-group">
                <label for="mobile_no" class="col-sm-2 control-label">Ketersediaan Template</label>

                <div class="col-sm-9">
                  <select class="form-control" name="ada_template" onchange="atur(this)">
                    <option value="1" <?= ($persyaratan['ada_template'] == 1)?'selected': '' ?> >Ada</option>
                    <option value="0" <?= ($persyaratan['ada_template'] == 0)?'selected': '' ?> >Tidak</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="url_template" class="col-sm-2 control-label">URL Template</label>

                <div class="col-sm-9">
                <textarea class="form-control" id="url_template" name="url_template"><?php echo ($this->input->post('url_template') ? $this->input->post('url_template') : $persyaratan['url_template']); ?></textarea>
                  <!--
                  <input type="file" name="url_template" value="<?php echo ($this->input->post('url_template') ? $this->input->post('url_template') : $persyaratan['url_template']); ?>" class="form-control" id="url_template" placeholder="URL Template">
                  -->
                </div>
              </div>
              <div class="form-group">
                <label for="jenis_file" class="col-sm-2 control-label">Jenis File</label>

                <div class="col-sm-9">
                  <input type="text" name="jenis_file" value="<?php echo ($this->input->post('jenis_file') ? $this->input->post('jenis_file') : $persyaratan['jenis_file']); ?>" class="form-control" id="jenis_file" placeholder="Jenis File">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 

<script src="<?php echo site_url('public/plugins/ckeditor/ckeditor.js') ?>"></script>
<script type="text/javascript">
  $(function () {
      CKEDITOR.replace('url_template',{
          //filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
          //filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
          //filebrowserImageBrowseUrl : '<?php echo base_url('public/kcfinder/browse.php');?>',
          //height: '400px'             
      });
  });
</script>
<script>
  $("#mn_syarat").addClass('active');
  $("#add_syarat").addClass('active');

  function atur(data) {
    var value = data.value;
    if (value === '1') {
      $("#jenis_file").attr('required', '');
    }else{
      $("#jenis_file").removeAttr('required', '');
    }
  };
</script>
<script>
$("#mn_syarat").addClass('active');

  function atur(data) {
    var value = data.value;
    if (value === '1') {
      $("#jenis_file").attr('required', '');
    }else{
      $("#jenis_file").removeAttr('required', '');
    }
  };
</script>    