<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit New Periode</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/periode/edit/'.$periode['id_periode']), 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="nama_periode" class="col-sm-2 control-label">Nama periode</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_periode" value="<?php echo ($this->input->post('nama_periode') ? $this->input->post('nama_periode') : $periode['nama_periode']); ?>" class="form-control" id="nama_periode" placeholder="Nama Periode">
                </div>
              </div>
              <div class="form-group">
                <label for="buka" class="col-sm-2 control-label">Buka</label>
                <div class="col-sm-9">
                  <input type="date" name="buka" value="<?php echo ($this->input->post('buka') ? $this->input->post('buka') : $periode['buka']); ?>" class="form-control" id="buka">
                </div>
              </div>
              <div class="form-group">
                <label for="tutup" class="col-sm-2 control-label">Tutup</label>
                <div class="col-sm-9">
                  <input type="date" name="tutup" value="<?php echo ($this->input->post('tutup') ? $this->input->post('tutup') : $periode['tutup']); ?>" class="form-control" id="tutup">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_periode").addClass('active');
$("#add_user").addClass('active');
</script>    