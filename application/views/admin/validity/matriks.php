<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Validasi
        <small>Matriks Penilaian</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Validasi</a></li>
        <li class="active">Mastriks</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Mastriks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Referensi</th>
                  <th>Type</th>
                  <th>Jawaban</th>
                  <th>File</th>
                  <th>Nilai</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($detail as $p) { ?>
                    <tr>
                      <td><?= $p['id_ref_penilaian'] ?></td>
                      <td><?= $p['jenis_standar'] ?></td>
                      <td><?= $p['jawaban'] ?></td>
                      <td><?= $p['url_file'] ?></td>
                      <td><?= $p['nilai_akhir'] ?></td>
                      <td>
                        <a class="btn btn-xs btn-info" href="<?php echo site_url('admin/pengajuan/detail_matriks/'.$p['id_isian']) ?>">Detail</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script>
  $("#mn_validasi").addClass('active');
</script>  
