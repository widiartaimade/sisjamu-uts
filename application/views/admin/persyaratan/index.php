 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  

 <section class="content">
   <div class="box">
    <div class="box-header">
      <h3 class="box-title">Persyaratan</h3>
      <div class="box-tools">
        <a class="btn btn-info float-right" href="<?php echo site_url('admin/persyaratan/add') ?>">Add</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>Kategori</th>
          <th>Nama Syarat</th>
          <th>Keterangan</th>
          <th>Template</th>
          <th>URL Template</th>
          <th>Jenis File</th>
          <th style="width: 150px;" class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($persyaratan as $row): ?>
          <tr>
            <td><?= $row['nama_kategori']; ?></td>
            <td><?= $row['nama_syarat']; ?></td>
            <td><?= $row['ket_syarat']; ?></td>
            <td><?= ($row['ada_template'] == 1)? 'Ada' : 'Tidak Ada' ?></td>
            <td><?= $row['url_template']; ?></td>
            <td><?= $row['jenis_file']; ?></td>
            <td class="text-right"><a href="<?= base_url('admin/persyaratan/edit/'.$row['id_persyaratan']); ?>" class="btn btn-info btn-flat">Edit</a><a href="<?= base_url('admin/persyaratan/delete/'.$row['id_persyaratan']); ?>" class="btn btn-danger btn-flat">Delete</a></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $("#mn_syarat").addClass('active');
  $("#view_syarat").addClass('active');
</script>        
