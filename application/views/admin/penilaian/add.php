<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add Ref Penilaian</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/ref_penilaian/add'), 'class="form-horizontal"');  ?> 
              <div class="form-group">
                <label for="standar" class="col-sm-2 control-label">Standar</label>

                <div class="col-sm-9">
                  <input type="text" name="standar" class="form-control" id="standar" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="butir" class="col-sm-2 control-label">Butir</label>

                <div class="col-sm-9">
                  <input type="text" name="butir" class="form-control" id="butir" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>

                <div class="col-sm-9">
                  <textarea name="deskripsi" class="form-control" id="deskripsi"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <button type="button" class="btn btn-info pull-left" onclick="goBack()">Cancel</button>
                </div>
                <div class="col-md-6">
                  <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 


<script>
$("#mn_matriks").addClass('active');
</script>    