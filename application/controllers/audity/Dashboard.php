<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends Audity_Contoller {
		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Setting_model');
			$this->load->model('admin/Periode_model');
		}

		public function index(){
			$data['periode_aktif'] = $this->Setting_model->getLimit();
			$data['periode'] = $this->Periode_model->periode_aktif();
			$data['sekarang'] = date('Y-m-d');

			$data['view'] = 'audity/dashboard/index';
			$this->load->view('audity/layout', $data);
		}

/*
		public function index2(){
			$data['view'] = 'audity/dashboard/index2';
			$this->load->view('audity/layout', $data);
		}

		public function pengajuanbaru(){
			$data['view'] = 'audity/dashboard/index2';
			$this->load->view('audity/layout', $data);
		}
*/
	}

?>	