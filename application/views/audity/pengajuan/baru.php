<!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/select2/select2.min.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Pengajuan Akreditasi Program Studi
        <small>Program Studi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pengajuan</a></li>
        <li class="active">Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Lengkapi form di bawah ini</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
		  
        <!-- /.box-header -->
      <?php echo form_open(base_url('audity/pengajuan/baru'), 'class="form-horizontal"');  ?> 
        <div class="box-body">
				  <div class="form-group">
            <label class="col-sm-3 control-label">Akreditasi 1/Akreditasi 2 :</label>
            <div class="col-sm-4">
              <select class="form-control select2" name="jenis_akreditasi">
                <option value="Akreditasi 1">Akreditasi 1</option>
                <option value="Akreditasi 2">Akreditasi 2</option>
              </select>
						</div>
          </div>
				  
				  <div class="form-group">
            <label  class="col-sm-3 control-label">Program Studi : </label>
            <div class="col-sm-4">
              <select class="form-control select2" name="prodi">
  						  <option selected="selected" value="Teknik Informatika">Teknik Informatika</option>
  						  <option value="Teknik Sipil">Teknik Sipil</option>
  						  <option value="Teknik Industri">Teknik Industri</option>
  						  <option value="Teknik Elektro">Teknik Elektro</option>
  						  <option value="Teknik Metalurgi">Teknik Metalurgi</option>
  						</select>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Jenjang : </label>
            <div class="col-sm-4">
              <select class="form-control select2" name="jenjang">
  						  <option selected="selected" value="S1">S1</option>
  						  <option value="S2">S2</option>
  						  <option value="S3">S3</option>
  						  <option value="D1">D1</option>
  						  <option value="D2">D2</option>
  						  <option value="D3">D3</option>
  						</select>
  					</div>
          </div>
				  
				  <div class="form-group">
            <label class="col-sm-3 control-label">Mode Pembelajaran :</label>
            <div class="col-sm-4">
              <select class="form-control select2" name="mode_pembelajaran">
                <option value="Tatap muka">Tatap muka</option>
                <option value="Jarak jauh">Jarak jauh</option>
              </select>
            </div>
          </div>
				  
				  <div class="form-group">
            <label  class="col-sm-3 control-label">Bidang Ilmu : </label>
  					<div class="col-sm-4">
  						<select class="form-control select2" name="bidang_ilmu">
  						  <option selected="selected" value="Teknik">Teknik</option>
  						  <option value="Psikologi">Psikologi</option>
  						  <option value="Ekonomi">Ekonomi</option>
  						  <option value="Bioteknologi">Bioteknologi</option>
  						  <option value="Komunikasi">Komunikasi</option>
  						</select>
  					</div>
          </div>
				  
				  <div class="form-group">
            <label  class="col-sm-3 control-label">No. Surat Pengantar : </label>
  				  <div class="col-sm-4">
  						<input type="text" class="form-control" name="no_srt_pengantar" placeholder="Maksimal 100 karakter">
  					</div>
          </div>
				  
				  <div class="form-group">
  					<label  class="col-sm-3 control-label">Alamat : </label>
  					<div class="col-sm-4">
  						<textarea class="form-control" rows="4" name="alamat" placeholder="Alamat ..."></textarea>
  					</div>
          </div>
				  
				  <div class="form-group">
  					<label  class="col-sm-3 control-label">Tanggal Surat : </label>
  					<div class="col-sm-4">
  						<input type="date" class="form-control" name="tgl_srt_pengantar">
  					</div>
          </div>
        </div>

        <div class="box-footer">
          <button type="button" class="btn btn-default">Cancel</button>
          <button type="submit" class="btn btn-info pull-right">Simpan</button>
        </div>
      </form>
    </div>
  </section>

<!-- Select2 -->
<script src="<?= base_url() ?>public/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url() ?>public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>public/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>public/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>public/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>public/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>public/plugins/iCheck/icheck.min.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>

<script>
  $("#pengajuan-baru").addClass('active');
</script>  
