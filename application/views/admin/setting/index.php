 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  

 <section class="content">
   <div class="box">
    <div class="box-header">
      <h3 class="box-title">Setting Periode Aktif</h3>
      <div class="box-tools">
        <a class="btn btn-info float-right" href="<?php echo site_url('admin/settings/add') ?>">Add</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>ID</th>
          <th>Kode Setting</th>
          <th>String</th>
          <th>INT</th>
          <th>Double</th>
          <th style="width: 150px;" class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($setting as $row): ?>
          <tr>
            <td><?= $row['id_setting']; ?></td>
            <td><?= $row['kode_setting']; ?></td>
            <td><?= $row['value_string']; ?></td>
            <td><?= $row['value_int']; ?></td>
            <td><?= $row['value_double']; ?></td>
            <td class="text-right"><a href="<?= base_url('admin/settings/edit/'.$row['id_setting']); ?>" class="btn btn-info btn-flat">Edit</a><a href="<?= base_url('admin/settings/delete/'.$row['id_setting']); ?>" class="btn btn-danger btn-flat">Delete</a></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $("#mn_setting").addClass('active');
</script>        
