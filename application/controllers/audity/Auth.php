<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('audity/auth_model', 'auth_model');
		}

		public function index(){
			if($this->session->has_userdata('is_audity_login'))
			{
				redirect('audity/dashboard');
			}
			else{
				redirect('audity/auth/login');
			}
		}

		public function login(){

			if($this->input->post('submit')){
				$this->form_validation->set_rules('email', 'Email', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->load->view('audity/auth/login');
				}
				else {
					$data = array(
					'email' => $this->input->post('email'),
					'password' => $this->input->post('password')
					);
					$result = $this->auth_model->login($data);
					if ($result == TRUE) {
						$audity_data = array(
							'audity_id' => $result['id_audity'],
						 	'name' => $result['username'],
						 	'is_audity_login' => TRUE
						);
						$this->session->set_userdata($audity_data);
						redirect(base_url('audity/dashboard'), 'refresh');
					}
					else{
						$data['msg'] = 'Invalid Email or Password!';
						$this->load->view('audity/auth/login', $data);
					}
				}
			}
			else{
				$this->load->view('audity/auth/login');
			}
		}	

		public function change_pwd(){
			$id = $this->session->userdata('audity_id');
			if($this->input->post('submit')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|matches[password]');
				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'audity/auth/change_pwd';
					$this->load->view('audity/layout', $data);
				}
				else{
					$data = array(
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
					);
					$result = $this->auth_model->change_pwd($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Password has been changed successfully!');
						redirect(base_url('audity/auth/change_pwd'));
					}
				}
			}
			else{
				$data['view'] = 'audity/auth/change_pwd';
				$this->load->view('audity/layout', $data);
			}
		}
				
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('audity/auth/login'), 'refresh');
		}
			
	}  // end class


?>