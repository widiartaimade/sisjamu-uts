<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Validasi_model', 'VM');
	}

	public function index()
	{
		$data['pengajuan'] = $this->VM->getAllPengajuan();
		$data['view'] = 'admin/validity/index';
		$this->load->view('admin/layout', $data);	
	}

	public function detail($id)
	{
		$data['detail'] = $this->VM->get_detail($id);
		$data['dokumen'] = $this->VM->getDoc($id);
		$data['view'] = 'admin/validity/detail';
		$this->load->view('admin/layout', $data);
	}

	public function update_status($id)
	{
		$data['detail'] = $this->VM->get_detail($id);

		if ($data['detail']['id_pengajuan']) {
			$params = array(
				'status_pengajuan' => $this->input->post('status_pengajuan'),
			);

			$this->VM->update_status($id, $params);
			$this->session->set_flashdata('msg', 'Status pengajuan has been updated');
			redirect('admin/pengajuan/detail/'.$data['detail']['id_pengajuan']);
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to update status does not exist');
			redirect('admin/pengajuan/detail/'.$data['detail']['id_pengajuan']);
		}
	}

	public function matriks($id)
	{
		$data['detail'] = $this->VM->get_matriks($id);

		if ($data['detail']['id_pengajuan']) {

			$data['view'] = 'admin/validity/matriks';
			$this->load->view('admin/layout', $data);

		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to view does not exist');
			redirect('admin/pengajuan/index');
		}
	}

	function verifikasi($id)
	{
		$data['data'] = $this->VM->getby_Id($id);

		if ($data['data']['id_dokumen']) {
			$params = array(
				'status_dokumen' => 'ok'
			);

			$this->VM->update($id, $params);
			$this->session->set_flashdata('msg', 'Dokumen has been verified');
			redirect('admin/pengajuan/detail/'.$data['data']['id_pengajuan']);
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to verified does not exist');
			redirect('admin/pengajuan/detail/'.$data['data']['id_pengajuan']);
		}
	}

	function revisi($id)
	{
		$data['data'] = $this->VM->getby_Id($id);

		if ($data['data']['id_dokumen']) {
			$params = array(
				'status_dokumen' => 'revisi'
			);

			$this->VM->update($id, $params);
			$this->session->set_flashdata('msg', 'Dokumen has been set to need revisi');
			redirect('admin/pengajuan/detail/'.$data['data']['id_pengajuan']);
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to set to need revisi does not exist');
			redirect('admin/pengajuan/detail/'.$data['data']['id_pengajuan']);
		}
	}

}

/* End of file pengajuan.php */
/* Location: ./application/controllers/admin/pengajuan.php */