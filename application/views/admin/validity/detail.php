<!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>public/plugins/select2/select2.min.css">

<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pengajuan Akreditasi Program Studi
        <small>Program Studi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pengajuan</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Detail</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
		  
        <!-- /.box-header -->
		  <form class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">Akreditasi Pertama/Reakreditasi :</label>
            <div class="col-sm-9">
  						<label>
  						  <input type="radio" name="r1" class="minimal" value="Akreditasi Pertama" <?= ($detail['jenis_akreditasi'] == 'Akreditasi Pertama')?'checked': '' ?> readonly >
  							Akreditasi Pertama
  						</label>
  						<label>
  						  <input type="radio" name="r1" class="minimal" value="Reakreditasi" <?= ($detail['jenis_akreditasi'] == 'Reakreditasi')?'checked': '' ?> readonly >
  							Reakreditasi
  						</label>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Program Studi : </label>
            <div class="col-sm-4">
              <select class="form-control select2" name="prodi" readonly>
  						  <option value="Teknik Informatika" <?= ($detail['prodi'] == 'Teknik Informatika')?'selected': '' ?> >Teknik Informatika</option>
  						  <option value="Teknik Sipil" <?= ($detail['prodi'] == 'Teknik Sipil')?'selected': '' ?> >Teknik Sipil</option>
  						  <option value="Teknik Industri" <?= ($detail['prodi'] == 'Teknik Industri')?'selected': '' ?> >Teknik Industri</option>
  						  <option value="Teknik Elektro" <?= ($detail['prodi'] == 'Teknik Elektro')?'selected': '' ?> >Teknik Elektro</option>
  						  <option value="Teknik Metalurgi" <?= ($detail['prodi'] == 'Teknik Metalurgi')?'selected': '' ?> >Teknik Metalurgi</option>
  						</select>
            </div>
          </div>
				  
          <div class="form-group">
            <label  class="col-sm-3 control-label">Jenjang : </label>
            <div class="col-sm-4">
  						<select class="form-control select" name="jenjang" readonly>
  						  <option value="S1" <?= ($detail['jenjang'] == 'S1')?'selected': '' ?> >S1</option>
  						  <option value="S2" <?= ($detail['jenjang'] == 'S2')?'selected': '' ?> >S2</option>
  						  <option value="S3" <?= ($detail['jenjang'] == 'S3')?'selected': '' ?> >S3</option>
  						  <option value="D1" <?= ($detail['jenjang'] == 'D1')?'selected': '' ?> >D1</option>
  						  <option value="D2" <?= ($detail['jenjang'] == 'D2')?'selected': '' ?> >D2</option>
  						  <option value="D3" <?= ($detail['jenjang'] == 'D3')?'selected': '' ?> >D3</option>
  						</select>
            </div>
          </div>
				  
				  <div class="form-group">
            <label class="col-sm-3 control-label">Modus Pembelajaran :</label>
            <div class="col-sm-4">
  						<label>
  						  <input type="radio" name="r2" class="minimal" value="Tatap muka" <?= ($detail['mode_pembelajaran'] == 'Tatap muka')?'checked': '' ?> readonly>
  							Tatap Muka
  						</label>
  						<label>
  						  <input type="radio" name="r2" class="minimal" value="Jarak jauh" <?= ($detail['mode_pembelajaran'] == 'Jarak jauh')?'checked': '' ?> readonly>
  							Jarak Jauh
  						</label>
            </div>
          </div>
				  
				  <div class="form-group">
  					<label  class="col-sm-3 control-label">Bidang Ilmu : </label>
  					<div class="col-sm-4">
  						<select class="form-control select2" name="bidang_ilmu" readonly>
  						  <option value="Teknik" <?= ($detail['bidang_ilmu'] == 'Teknik')?'selected': '' ?> >Teknik</option>
  						  <option value="Psikologi" <?= ($detail['bidang_ilmu'] == 'Psikologi')?'selected': '' ?> >Psikologi</option>
  						  <option value="Ekonomi" <?= ($detail['bidang_ilmu'] == 'Ekonomi')?'selected': '' ?> >Ekonomi</option>
  						  <option value="Bioteknologi" <?= ($detail['bidang_ilmu'] == 'Bioteknologi')?'selected': '' ?> >Bioteknologi</option>
  						  <option value="Komunikasi" <?= ($detail['bidang_ilmu'] == 'Komunikasi')?'selected': '' ?> >Komunikasi</option>
  						</select>
            </div>
          </div>
				  
				  <div class="form-group">
            <label  class="col-sm-3 control-label">No. Surat Pengantar : </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="no_surat" value="<?= $detail['no_srt_pengantar'] ?>" readonly>
            </div>
          </div>
				  
				  <div class="form-group">
  					<label  class="col-sm-3 control-label">Alamat : </label>
  					<div class="col-sm-4">
  						<textarea class="form-control" rows="4" readonly><?= $detail['alamat'] ?></textarea>
  					</div>
          </div>
				  
				  <div class="form-group">
            <label  class="col-sm-3 control-label">Tanggal Surat  : </label>
						<div class="input-group date col-sm-4">
						  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input type="text" class="form-control pull-right" id="datepicker" value="<?= $detail['tgl_srt_pengantar'] ?>" readonly>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Jadwal AK</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="jdwl_ak" value="<?= $detail['jdwl_ak'] ?>" readonly>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Jadwal AL</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="jdwl_al" value="<?= $detail['jdwl_al'] ?>" readonly>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Hasil AK</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="hasil_ak" value="<?= $detail['hasil_ak'] ?>" readonly>
            </div>
          </div>

          <div class="form-group">
            <label  class="col-sm-3 control-label">Hasil AL</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="hasil_al" value="<?= $detail['hasil_al'] ?>" readonly>
            </div>
          </div>

        </div>
      </form>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

	<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Kelengkapan Dokumen</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>

    <div class="box-body">  
      <div class="box-body table-responsive no-padding">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
          <tr>
            <!--<th>ID Persyaratan</th>-->
            <th>Tgl Upload</th>
            <th>Nama Dokumen</th>
            <th>URL</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
            <?php foreach ($dokumen as $d) { ?>
              <tr>
                <!--<td><?= $d['id_perysaratan'] ?></td>-->
                <td><?= $d['tgl_upload'] ?></td>
                <td><?= $d['nama_dokumen'] ?></td>
                <td><a href="<?= base_url(''.$d['url_file']) ?>"><i class="fa fa-download"></i></a></td>
                <td><p class="label label-info"><?= $d['status_dokumen'] ?></p></td>
                <td>
                  <a href="<?php echo site_url('admin/pengajuan/revisi/'.$d['id_dokumen']) ?>" class="btn btn-xs btn-danger <?= ($d['status_dokumen'] == 'revisi')? 'disabled': ''?>">Revisi</a>
                  <a href="<?php echo site_url('admin/pengajuan/verifikasi/'.$d['id_dokumen']) ?>" class="btn btn-xs btn-info <?= ($d['status_dokumen'] == 'ok')? 'disabled': ''?>">OK</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <?php echo form_open(base_url('admin/pengajuan/update_status/'.$detail['id_pengajuan']), 'class="form-horizontal"' )?>
    <div class="box box-default">
      <div class="box-header">
        <h4 class="box-title">Update status pengajuan</h4>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">          
      <div class="form-group">
        <label  class="col-sm-3 control-label">Status Pengajuan : </label>
        <div class="col-sm-4">
          <select class="form-control select2" name="status_pengajuan">
            <option value="Submit Dokumen" <?= ($detail['status_pengajuan'] == 'Submit Dokumen')?'selected': '' ?> >Submit Dokumen</option>
            <option value="Revisi Dokumen" <?= ($detail['status_pengajuan'] == 'Revisi Dokumen')?'selected': '' ?> >Revisi Dokumen</option>
            <option value="Menunggu proses AK" <?= ($detail['status_pengajuan'] == 'Menunggu proses AK')?'selected': '' ?> >Menunggu proses AK</option>
            <option value="Proses AK" <?= ($detail['status_pengajuan'] == 'Proses AK')?'selected': '' ?> >Proses AK</option>
            <option value="Tidak Lulus AK" <?= ($detail['status_pengajuan'] == 'Tidak Lulus AK')?'selected': '' ?> >Tidak Lulus AK</option>
            <option value="Tunda AL" <?= ($detail['status_pengajuan'] == 'Tunda AL')?'selected': '' ?> >Tunda AL</option>
            <option value="Proses AL" <?= ($detail['status_pengajuan'] == 'Proses AL')?'selected': '' ?> >Proses AL</option>
            <option value="Waiting Hasil AL" <?= ($detail['status_pengajuan'] == 'Waiting Hasil AL')?'selected': '' ?> >Waiting Hasil AL</option>
            <option value="Waiting SK" <?= ($detail['status_pengajuan'] == 'Waiting SK')?'selected': '' ?> >Waiting SK</option>
            <option value="Terbit SK & Sertifikat" <?= ($detail['status_pengajuan'] == 'Terbit SK & Sertifikat')?'selected': '' ?> >Terbit SK & Sertifikat</option>
            <option value="Tekirim SK & Sertifikat" <?= ($detail['status_pengajuan'] == 'Tekirim SK & Sertifikat')?'selected': '' ?> >Tekirim SK & Sertifikat</option>
            <option value="Tidak terakreditasi" <?= ($detail['status_pengajuan'] == 'Tidak terakreditasi')?'selected': '' ?> >Tidak terakreditasi</option>
          </select>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <!--<button class="btn btn-info pull-left" onclick="goBack()">Back</button>-->
      <button type="submit" class="btn btn-success pull-right">Update</button>
    </div>
    </div>
  </form>

  </section>

<!-- Select2 -->
<script src="<?= base_url() ?>public/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>public/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url() ?>public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>public/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>public/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>public/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>public/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>public/plugins/iCheck/icheck.min.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  
