<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('kategori_syarat')->result_array();
	}

	public function insert($params)
	{
		return $this->db->insert('kategori_syarat', $params);
	}

	public function getby_Id($id)
	{
		$this->db->where('id_kategori_syarat', $id);
		return $this->db->get('kategori_syarat')->row_array();
	}

	public function update($id, $params)
	{
		$this->db->where('id_kategori_syarat', $id);
		return $this->db->update('kategori_syarat', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_kategori_syarat', $id);
		return $this->db->delete('kategori_syarat');
	}

}

/* End of file Kategori_model.php */
/* Location: ./application/models/admin/Kategori_model.php */