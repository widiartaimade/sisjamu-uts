<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_auditor_model extends CI_Model {

	public function getAll()
	{
		$this->db->join('auditor', 'auditor.id_auditor = auditor_pangajuan.id_auditor', 'left');
		return $this->db->get('auditor_pangajuan')->result_array();
	}

	public function getPengajuan()
	{
		return $this->db->get('pengajuan')->result_array();
	}	

	public function getby_Id($id)
	{
		return $this->db->get_where('auditor_pangajuan', array('id_auditor_pengajuan' => $id))->row_array();
	}

	public function update($id, $params)
	{
		$this->db->where('id_auditor_pengajuan', $id);
		return $this->db->update('auditor_pangajuan', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_auditor_pengajuan', $id);
		return $this->db->delete('auditor_pangajuan');
	}

}

/* End of file Pengajuan_auditor_model.php */
/* Location: ./application/models/admin/Pengajuan_auditor_model.php */