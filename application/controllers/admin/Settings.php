<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Setting_model', 'SM');
		$this->load->model('admin/Periode_model', 'PM');
	}

	public function index()
	{
		$id = '1';
		$data['setting'] = $this->SM->getby_Id($id);
		
		if ($data['setting']['id_setting']) {
			$this->form_validation->set_rules('id_periode', 'Periode', 'trim|required');
			if ($this->form_validation->run()) {
				$params = array(
					'value_int' => $this->input->post('id_periode')
				);
				$this->SM->update($id, $params);
				$this->session->set_flashdata('msg', 'Periode has been update');
				redirect('admin/settings');
			} else {
				$data['aktif'] = $this->SM->getAll();
				$data['periode'] = $this->PM->get_all_periode();
				$data['view'] = 'admin/setting/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to edit does not exist');
			redirect('admin/settings');
		}
	}

	public function index2()
	{
		$data['setting'] = $this->SM->getAll();
		$data['view'] = 'admin/setting/index';
		$this->load->view('admin/layout', $data);
	}

	public function edit($id)
	{
		$data['setting'] = $this->SM->getby_Id($id);
		
		if ($data['setting']['id_setting']) {
			$this->form_validation->set_rules('id_periode', 'Periode', 'trim|required');
			if ($this->form_validation->run()) {
				$params = array(
					'value_int' => $this->input->post('id_periode')
				);
				$this->SM->update($id, $params);
				$this->session->set_flashdata('msg', 'Periode has been update');
				redirect('admin/settings');
			} else {
				$data['periode'] = $this->PM->get_all_periode();
				$data['view'] = 'admin/setting/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to edit does not exist');
			redirect('admin/settings');
		}
	}

	public function add()
	{
		$this->form_validation->set_rules('id_periode', 'Periode', 'trim|required');
		if ($this->form_validation->run()) {
			$params = array(
				'value_int' => $this->input->post('periode')
			);
			$this->SM->insert($params);
			$this->session->set_flashdata('msg', 'Periode has been add');
			redirect('admin/settings');
		} else {
			$data['periode'] = $this->PM->get_all_periode();
			$data['view'] = 'admin/setting/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function delete($id)
	{
		$data['setting'] = $this->SM->getby_Id($id);
		
		if ($data['setting']['id_setting']) {
			
				$this->SM->delete($id);
		}else{
			$this->session->set_flashdata('msg', 'Data you are trying to delete does not exist');
			redirect('admin/settings');
		}
	}

}

/* End of file Settings.php */
/* Location: ./application/controllers/admin/Settings.php */