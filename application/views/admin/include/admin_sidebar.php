<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li id="dashboard">
          <a href="<?= base_url('admin/dashboard'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu">
        <li id="mn_users" class="treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>Manajemen Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="mn_admin"><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-circle-o"></i> Admin</a></li>
              <li id="mn_audity" class=""><a href="<?= base_url('admin/audity'); ?>"><i class="fa fa-circle-o"></i> Audity</a></li>
              <li id="mn_auditor" class=""><a href="<?= base_url('admin/auditor'); ?>"><i class="fa fa-circle-o"></i> Auditor</a></li>
            </ul>
          </li>

          <li id="mn_periode" class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>Periode</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_periode"><a href="<?= base_url('admin/periode/add'); ?>"><i class="fa fa-circle-o"></i> Add Periode</a></li>
              <li id="view_periode" class=""><a href="<?= base_url('admin/periode'); ?>"><i class="fa fa-circle-o"></i> View Periode</a></li>
            </ul>
          </li>

          <li id="mn_kategori" class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>Kategori persyaratan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_kategori"><a href="<?= base_url('admin/kategori/add'); ?>"><i class="fa fa-circle-o"></i> Add kategori</a></li>
              <li id="view_kategori" class=""><a href="<?= base_url('admin/kategori'); ?>"><i class="fa fa-circle-o"></i> View kategori</a></li>
            </ul>
          </li>

          <li id="mn_syarat" class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>Persyaratan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_syarat"><a href="<?= base_url('admin/persyaratan/add'); ?>"><i class="fa fa-circle-o"></i> Add Persyaratan</a></li>
              <li id="view_syarat" class=""><a href="<?= base_url('admin/persyaratan'); ?>"><i class="fa fa-circle-o"></i> View Persyaratan</a></li>
            </ul>
          </li>
          <li id="mn_setting">
            <a href="<?= base_url('admin/settings'); ?>">
              <i class="fa fa-gear"></i> <span>Set Periode Aktif</span>
            </a>
          </li>
          <li id="mn_validasi">
            <a href="#">
              <i class="fa fa-envelope"></i> <span>Pengajuan</span>
            </a>
            <ul class="treeview-menu">
              <li id="all_pengajuan"><a href="<?= base_url('admin/pengajuan'); ?>"><i class="fa fa-circle-o"></i> Pengajuan AK/AL</a></li>
              <li id="mn_penugasan"><a href="<?= base_url('admin/pengajuan_auditor'); ?>"><i class="fa fa-circle-o"></i> Auditor</a></li>
              <li id="all_matriks" class=""><a href="<?= base_url('admin/hasil_assessment'); ?>"><i class="fa fa-circle-o"></i> Mastriks Penilaian</a></li>
            </ul>
          </li>
          <li id="mn_matriks">
            <a href="<?= base_url('admin/ref_penilaian'); ?>">
              <i class="fa fa-circle"></i> <span>Ref Matriks Penilaian</span>
            </a>
          </li>
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>
