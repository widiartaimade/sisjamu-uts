<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_model extends CI_Model {

	public function get_all_periode()
	{
		return $this->db->get('periode')->result_array();
	}

	public function periode_aktif()
	{
		$tgl = date('Y-m-d');

		$satu = "SELECT * FROM periode WHERE buka BETWEEN '$tgl' AND '$tgl'";
		$dua = "SELECT * FROM periode WHERE tutup BETWEEN '$tgl' AND '$tgl'";

		return $this->db->query($satu.' UNION '.$dua)->row_array();

		//return $this->db->query("SELECT * FROM periode WHERE buka BETWEEN '$tgl' AND '$tgl' AND tutup BETWEEN '$tgl' AND '$tgl'")->result_array();
	}

	public function get_periode_aktif()
	{
		$tgl = date('Y-m-d');

		$this->db->where('buka >=', $tgl);
		$this->db->where('tutup <=', $tgl);
		return $this->db->get('periode')->result_array();
	}

	public function get_byId($id)
	{
		return $this->db->get_where('periode', array('id_periode' => $id))->row_array();
	}

	public function insert($params)
	{
		return $this->db->insert('periode', $params);;
	}

	public function update($params, $id)
	{
		$this->db->where('id_periode', $id);
		return $this->db->update('periode', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_periode', $id);
		return $this->db->delete('periode');
	}

}

/* End of file Periode_model.php */
/* Location: ./application/models/admin/Periode_model.php */