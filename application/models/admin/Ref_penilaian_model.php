<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_penilaian_model extends CI_Model {

	public function getAll()
	{
		return $this->db->get('ref_matriks_penilaian')->result_array();
	}

	public function insert($params)
	{
		return $this->db->insert('ref_matriks_penilaian', $params);
	}

	public function getby_Id($id)
	{
		return $this->db->get_where('ref_matriks_penilaian', array('id_ref_m_penilaian', $id))->row_array();
	}

	public function update($id, $params)
	{
		$this->db->where('id_ref_m_penilaian', $id);
		return $this->db->update('ref_matriks_penilaian', $params);
	}

	public function delete($id)
	{
		$this->db->where('id_ref_m_penilaian', $id);
		return $this->db->delete('ref_matriks_penilaian');
	}

}

/* End of file Ref_penilaian_model.php */
/* Location: ./application/models/admin/Ref_penilaian_model.php */