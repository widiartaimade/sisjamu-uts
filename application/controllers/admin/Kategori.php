<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Kategori_model', 'KM');
	}

	public function index()
	{
		$data['kategori'] = $this->KM->getAll();
		$data['view'] = 'admin/kategori/index';
		$this->load->view('admin/layout', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required');
		$this->form_validation->set_rules('ket_kategori', 'Keterangan', 'trim|required');

		if ($this->form_validation->run()) {
			$params = array(
				'nama_kategori' => $this->input->post('nama_kategori'),
				'ket_kategori' => $this->input->post('ket_kategori')
			);

			$result = $this->KM->insert($params);
			if ($result) {
				$this->session->set_flashdata('msg', 'Record added succesfully');
				redirect('admin/kategori');
			}else{
				$this->session->set_flashdata('msg', 'Record failed to insert');
				redirect('admin/kategori/add');
			}
		} else {
			$data['view'] = 'admin/kategori/add';
			$this->load->view('admin/layout', $data);
		}
	}

	public function edit($id)
	{
		$data['kategori'] = $this->KM->getby_Id($id);

		if ($data['kategori']['id_kategori_syarat']) {

			$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required');
			$this->form_validation->set_rules('ket_kategori', 'Keterangan', 'trim|required');

			if ($this->form_validation->run()) {
				$params = array(
					'nama_kategori' => $this->input->post('nama_kategori'),
					'ket_kategori' => $this->input->post('ket_kategori')
				);

				$this->KM->update($id, $params);
				$this->session->set_flashdata('msg', 'Data succesfully update');
				redirect('admin/kategori');
			} else {
				$data['view'] = 'admin/kategori/edit';
				$this->load->view('admin/layout', $data);
			}
		}else{
			$this->session->set_flashdata('msg', 'Data was you are trying to edit does not exist');
			redirect('admin/kategori');
		}
	}

	public function delete($id)
	{
		$data['kategori'] = $this->KM->getby_Id($id);

		if ($data['kategori']['id_kategori_syarat']) {
			$this->KM->delete($id);
			$this->session->set_flashdata('msg', 'Data succesfully deleted');
			redirect('admin/kategori');
		}else{
			$this->session->set_flashdata('msg', 'Data was you trying to delete does not exist');
			redirect('admin/kategori');
		}
	}

}

/* End of file Kategori.php */
/* Location: ./application/controllers/admin/Kategori.php */