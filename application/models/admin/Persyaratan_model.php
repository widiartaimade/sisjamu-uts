<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persyaratan_model extends CI_Model {

	public function getAll()
	{
		$this->db->join('kategori_syarat', 'kategori_syarat.id_kategori_syarat = persyaratan.id_kategori_syarat', 'left');
		return $this->db->get('persyaratan')->result_array();
	}

	public function insert($params)
	{
		return $this->db->insert('persyaratan', $params);
	}

	public function update($id, $params)
	{
		$this->db->where('id_persyaratan', $id);
		return $this->db->update('persyaratan', $params);
	}

	public function getby_Id($id)
	{
		$this->db->where('id_persyaratan', $id);
		return $this->db->get('persyaratan')->row_array();
	}

	public function delete($id)
	{
		$this->db->where('id_persyaratan', $id);
		return $this->db->delete('persyaratan');
	}

}

/* End of file Persyaratan_model.php */
/* Location: ./application/models/admin/Persyaratan_model.php */